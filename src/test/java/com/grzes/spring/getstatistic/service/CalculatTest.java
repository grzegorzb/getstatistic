package com.grzes.spring.getstatistic.service;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class CalculatTest {
    private final Calculat calc = new Calculat();

    @Test
    public void  testCheckParam() {
        calc.checkParam("23");
        calc.checkParam("14");
        calc.checkParam("8");
        assertEquals(3,calc.getCntVal());
        assertEquals(3.0,calc.getMinuteAvg());
        assertEquals(15.0,calc.getValueAvg());
    }
}