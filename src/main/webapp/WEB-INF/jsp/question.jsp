<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
.container {
  height: 20px;
  position: relative;
}
</style>
<table>
    <tr>
         <th> <h3>Żądanie wysłano: <%= java.time.LocalTime.now().toString() %> </h3> </th>
    </tr>
    <tr>
        <th> <h4>Podałeś parametr:</h4> </th>  <th>  </th>  <th> <h4>${load_num}</h4> </th>
    </tr>
    <tr></tr>
    <tr class="container"><td>Całkowita liczba wywołań: </td>  <td>  </td>  <td> ${count_all} </td></tr>
    <tr class="container"><td>Średnia ilość wywołań na minutę: </td>  <td>  </td>  <td> ${minute_avg} </td></tr>
    <tr class="container"><td>Średnia z wartości parametru przekazywanego w wywołaniu: </td>  <td>  </td>  <td> ${value_avg} </td></tr>
</table>