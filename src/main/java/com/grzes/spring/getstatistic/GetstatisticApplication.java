package com.grzes.spring.getstatistic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GetstatisticApplication {

	public static void main(String[] args) {
		SpringApplication.run(GetstatisticApplication.class, args);
	}

}
