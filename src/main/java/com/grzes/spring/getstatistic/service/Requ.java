package com.grzes.spring.getstatistic.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.stereotype.Component;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@AllArgsConstructor @Getter
public class Requ {
    private LocalDateTime lt;
    private int val;

    @Override
    public String toString() {
        return "Request{ DateTime= " + lt.format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm")) +
                ", val= " + val + '}';
    }
}
