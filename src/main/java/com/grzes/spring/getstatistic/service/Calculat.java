package com.grzes.spring.getstatistic.service;

import com.grzes.spring.getstatistic.config.ConfigProp;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

@Getter     @Setter     @EqualsAndHashCode      @NoArgsConstructor
@Component
public class Calculat {

    private ConfigProp configProp = new ConfigProp();
    List<Requ> lReq = new LinkedList<>();
    private double minuteAvg = 0.0;
    private double valueAvg = 0.0;
    private int cntVal = 0;
    private String valS;

    public void checkParam(String valStr) {
//        System.out.println("Jestem w  checkParam ** valStr= " + valStr);
        int val;
        try {
            val = Integer.parseInt(valStr);
        } catch (NumberFormatException ex) {
            valS = "Zly parametr: " + valStr;
            return;
        }
        if (val >= configProp.getMin() && val <= configProp.getMax()) {
            valS = valStr;
            ++cntVal;
            lReq.add(new Requ(LocalDateTime.now(), Integer.valueOf(val)));
            valueAvg = lReq.stream()
                    .mapToInt(x -> x.getVal())
                    .average()
                    .getAsDouble();
            minuteAvg = lReq.stream()
                    .collect(groupingBy(t-> t.getLt().format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm")), counting()))
                    .values().stream()
                    .mapToInt(x-> x.intValue())
                    .average()
                    .getAsDouble();
//            System.out.println( "AllStream: "+ lReq.stream().map(t->t.toString()).collect(Collectors.joining(", "))  );
        } else {
            valS = "Parametr poza zdefiniowanego zakresu od " + configProp.getMin() + " do " + configProp.getMax();
        }
    }
}
