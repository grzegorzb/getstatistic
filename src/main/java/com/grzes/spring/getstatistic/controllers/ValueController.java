package com.grzes.spring.getstatistic.controllers;

import com.grzes.spring.getstatistic.service.Calculat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Controller
public class ValueController {
    private final Calculat calc = new Calculat();

    @GetMapping("/{val}")
    public String getParamValue(@PathVariable("val") String valStr, Model model) {
        calc.checkParam(valStr);

        model.addAttribute("load_num", calc.getValS());
        model.addAttribute("count_all", calc.getCntVal());
        model.addAttribute("minute_avg", calc.getMinuteAvg());
        model.addAttribute("value_avg", calc.getValueAvg());
        return "question";
    }


}
