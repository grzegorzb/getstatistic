package com.grzes.spring.getstatistic.controllers;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@JsonSerialize
public class JsonResponse {
    private String minuteAvg ="0.0";
    private String valueAvg = "0.0";
    private String cntVal = "0";

    @Override
    public String toString() {
        return "{ cntVal:" + cntVal +
                ", minuteAvg:" + minuteAvg +
                ", valueAvg:" + valueAvg + " }";
    }
}
