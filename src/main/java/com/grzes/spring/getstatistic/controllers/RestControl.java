package com.grzes.spring.getstatistic.controllers;

import com.grzes.spring.getstatistic.service.Calculat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class RestControl {
        private final Calculat calc = new Calculat();

        @RequestMapping(value = "/api/{val}", method = RequestMethod.GET)
        public ResponseEntity<String> retJson(@PathVariable("val") String valStr) {
            calc.checkParam(valStr);
            JsonResponse resp = new JsonResponse(String.valueOf(calc.getMinuteAvg()),String.valueOf(calc.getValueAvg()),String.valueOf(calc.getCntVal()));
            return new ResponseEntity<>(resp.toString(), HttpStatus.OK);
        }

}
