package com.grzes.spring.getstatistic.config;


import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.List;

@Getter @NoArgsConstructor
@Component
public class ConfigProp {

    private final int min=0;
    private final int max=100;
}
